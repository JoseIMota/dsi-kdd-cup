# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_absolute_error
import matplotlib.pyplot as plt
from sklearn import preprocessing
from sklearn import mixture
from sklearn.cluster import KMeans
from sklearn import neighbors
from sklearn.cross_validation import cross_val_score

#CARGA DE DATOS
print('Cargando datos...')
original_data = pd.read_csv('algebra_2005_2006_train.txt', sep='\t')   
print('Separando estadisticas de alumnos de los datos originales')
studentStatistics = original_data.drop(columns=['Row', 'Problem Hierarchy', 'Problem Name', 
                                                'Problem View', 'Step Name', 'Step Start Time', 
                                                'First Transaction Time', 'Correct Transaction Time', 
                                                'Step End Time', 'Step Duration (sec)', 
                                                'Correct Step Duration (sec)', 
                                                'Error Step Duration (sec)', 
                                                'KC(Default)', 'Opportunity(Default)'])  

print ("Varianza: ")
print (studentStatistics.var())

print ("Maximos: ")
print (studentStatistics.max())

print('Normalizando datos para clustering')
aux = studentStatistics.iloc[:,1:5].values 
min_max_scaler = preprocessing.MinMaxScaler()
x_scaled = min_max_scaler.fit_transform(aux)

del aux

studentStatistics = pd.DataFrame(studentStatistics.iloc[:,0]).join(pd.DataFrame(x_scaled))
studentStatistics.columns = ["id", "CFA", "I", "H", "C"]

del x_scaled

print ("Varianza: ")
print (studentStatistics.var())

print ("Maximos: ")
print (studentStatistics.max())   

print('Agrupando datos por alumno')
names = studentStatistics['id'].unique()

result = pd.DataFrame(columns = ["CFAM", "IM", "HM", "CM", "CFAV", "IV", "HV", "CV"])

for i in range(0,len(names)):
    aux = studentStatistics.loc[studentStatistics['id'] == names[i]]
    means = pd.DataFrame(aux.mean()).transpose()
    means.columns = ["CFAM", "IM", "HM", "CM"]
    variances = pd.DataFrame(aux.var()).transpose()
    variances.columns = ["CFAV", "IV", "HV", "CV"]
    union = means.join(variances)
    result = result.append(union, ignore_index = True)

del studentStatistics

#BIC nos permitirá obtener el número óptimo de clusters    
print('calculando BIC')
lowest_bic = np.infty
bic = []

n_components_range = range(1, 10)
cv_types = ['spherical', 'tied', 'diag', 'full']
for cv_type in cv_types:
    for n_components in n_components_range:
        gmm = mixture.GaussianMixture(n_components=n_components,
                                      covariance_type=cv_type)
        gmm.fit(result)
        bic.append(gmm.bic(result))
        if bic[-1] < lowest_bic:
            lowest_bic = bic[-1]
            best_gmm = gmm
print('Numero óptimo de clusters = '+str(best_gmm.n_components)+', Tipo de covarianza = '+ str(best_gmm.covariance_type))
print('Calculando clusters')
kmeans = KMeans(n_clusters=best_gmm.n_components, random_state=0, init='k-means++', n_init=10, max_iter=300).fit(result)

print('Asignando clusters a cada alumno en datos originales')
clusters = pd.DataFrame(kmeans.predict(result), columns=['cluster'])
names = pd.DataFrame(names, columns=['Anon Student Id'])
clusters = names.join(clusters)
original_data = pd.merge(original_data, clusters, how='left', on=['Anon Student Id'])

#Codificamos los datos categóricos para poderlos utilizar en la predicción

print('Codificando Student ID')
aux = names.join(pd.DataFrame(names.index.values, columns = ['Alumno']))
original_data = pd.merge(original_data, aux, how='left', on=['Anon Student Id'])

print('Codificando Problem Hierarchy')
aux = pd.DataFrame(original_data['Problem Hierarchy'].unique(), columns = ['Problem Hierarchy'])
aux = aux.join(pd.DataFrame(aux.index.values, columns = ['Jerarquia']))
original_data = pd.merge(original_data, aux, how='left', on=['Problem Hierarchy'])

print('Codificando Problem Name')
aux = pd.DataFrame(original_data['Problem Name'].unique(), columns = ['Problem Name'])
aux = aux.join(pd.DataFrame(aux.index.values, columns = ['Problema']))
original_data = pd.merge(original_data, aux, how='left', on=['Problem Name'])

print('Codificando Step Name')
aux = pd.DataFrame(original_data['Step Name'].unique(), columns = ['Step Name'])
aux = aux.join(pd.DataFrame(aux.index.values, columns = ['Paso']))
original_data = pd.merge(original_data, aux, how='left', on=['Step Name'])

print('Codificando KC')
aux = pd.DataFrame(original_data['KC(Default)'].unique(), columns = ['KC(Default)'])
aux = aux.join(pd.DataFrame(aux.index.values, columns = ['Habilidades']))
original_data = pd.merge(original_data, aux, how='left', on=['KC(Default)'])

print('Codificando Oportunity')
aux = pd.DataFrame(original_data['Opportunity(Default)'].unique(), columns = ['Opportunity(Default)'])
aux = aux.join(pd.DataFrame(aux.index.values, columns = ['Oportunidad']))
original_data = pd.merge(original_data, aux, how='left', on=['Opportunity(Default)'])


print('Preparando los campos necesarios para la regresión')
#Sólo podemos tratar con campos numéricos, así que eliminamos los categóricos
data = original_data.drop(columns=['Row', 'Anon Student Id', 'Problem Hierarchy', 'Problem Name',
                                   'Step Name', 'Step Start Time', 'First Transaction Time', 
                                   'Correct Transaction Time', 'Step End Time', 
                                   'Step Duration (sec)', 'Correct Step Duration (sec)', 
                                   'Error Step Duration (sec)', 'Correct First Attempt',
                                   'KC(Default)', 'Opportunity(Default)',
                                   'Problem View', 'Incorrects', 'Hints', 'Corrects'
                                   ])

print('Normalizando datos para la regresión')    
data_scaled = pd.DataFrame(min_max_scaler.fit_transform(data.values))
#Esta primera división es para reducir el volumen de datos, si no la ejecución toma HORAS
x_train, x_test, y_train, y_test = train_test_split(data_scaled, original_data.iloc[:,13], test_size=0.9976, random_state=0)
#Esta división es la buena ;)
x_train, x_test, y_train, y_test = train_test_split(x_train, y_train, test_size=0.25, random_state=0)

#Liberamos memoria
del result
del aux
del names
del clusters
del data
del data_scaled
del original_data


#PARAMETRIZACIÓN
"""#1. Parametrización

Existen diferentes parámetros para método basado en los vecinos más cercanos utilizando scikit-learn

*   **KNeighbors/Radius**: Elegimos KNeighbors porque los datos están muestreados de forma uniforme
    * k: Un número k mayor suprime el eecto del ruido pero hace a los límites de clasificación más distintos.
    * Radios, un radio fijo es muy adecuado cuando los datos están muy dispersos (sprarse neighboors
*   **Pesos*: dos posibles valores, "uniform" cada vecino tiene el mismo peso, weights se asigna un peso a cada 
              vecino proporcional a la distancia que esté del elemento referencia.

Nuestra elección KNeighbors y k y pesos se van a parametrizar para ello se ejecutará validación cruzada.
"""
print('Preparando la parametrización de KNN')
for i, weights in enumerate(['uniform', 'distance']):
    total_scores = []
    for n_neighbors in range(1,30):
        knn = neighbors.KNeighborsRegressor(n_neighbors, weights=weights)
        knn.fit(x_train,y_train)
        scores = -cross_val_score(knn, x_train,y_train, 
                                    scoring='neg_mean_absolute_error', cv=10)
        total_scores.append(scores.mean())
    
    plt.plot(range(1,len(total_scores)+1), total_scores, 
             marker='o', label=weights)
    plt.ylabel('cv score')
plt.legend()
plt.show()

print('Calculando la regresión')
# x axis for plotting
xx = np.stack(i for i in range(len(y_test)))

n_neighbors = 2 # Obtenido de la gráfica anterior
for i, weights in enumerate(['uniform', 'distance']):
    knn = neighbors.KNeighborsRegressor(n_neighbors, weights=weights)
    y_pred = knn.fit(x_train,y_train).predict(x_test) 
    y_pred = y_pred.round(decimals=0).astype(int)# redondeamos el resultado, necesitamos valores 0 ó 1
    mae = mean_absolute_error(y_test,y_pred)
    
    plt.figure(figsize=(20,3)) #ensanchamos la gráfica para poder ver los resultados
    plt.scatter(xx, y_test, c='tab:blue', marker = 'X', label='data')
    plt.plot(xx, y_pred, c='tab:orange', label='prediction')
    plt.axis('tight')
    plt.legend()
    plt.title("KNeighborsRegressor (k = %i, weights = '%s')" % (n_neighbors, weights))
    plt.show()
    print ("Error Measure ", weights, mae)























