# README #

### What is this repository for? ###
* Repositorio para alojar los ficheros correspondientes a la tarea final de la asignatura de Diseño de Sistemas Inteligentes
* El sistema trabaja con los datos de la competición KDD-Cup
* El sistema pretende caracterizar a los alumnos según su rendimiento y predecir si un alumno conseguirá completar correctamente a la primera un paso de un problema teniendo en cuenta su desempeño en problemas similares.